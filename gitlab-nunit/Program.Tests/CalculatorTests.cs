using NUnit.Framework;
using Program;
    
namespace program.tests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator calculator;
            
        [SetUp]
        public void SetUp() {
            calculator = new Calculator();
        }

        [Test]
        public void Add_OnePlusOne_ReturnsTwo()
        {
            var expected = 2;
            var actual = calculator.Add(1, 1);
                
            Assert.AreEqual(expected, actual);
        }
    }
}