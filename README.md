### Integrating NUnit tests with Obsidian using Gitlab CI/CD

This guide will walk you through setting up a new .NET Core project using the .NET CLI, adding a new test using NUnit, and configuring a new `.gitlab-ci.yml` file that runs your tests and upload the results to Obsidian QA.


Before we start make sure you have done the following:
1) Created a new empty repository in Gitlab
2) Installed the .NET SDK which includes the .NET CLI we will use in this tutorial

#### New Solution Setup

Once you've completed the steps above, clone your new Gitlab repo to your machine, navigate to the root folder, and run the following commands:
1) `dotnet new sln -o gitlab-nunit`
2) `cd gitlab-nunit`
3) `dotnet new classlib -o Program`
4) `dotnet sln add ./Program/Program.csproj`
5) `dotnet new nunit -o Program.Tests`
6) `dotnet sln add ./Program.Tests/Program.Tests.csproj`
7) `dotnet add Program.Tests reference Program`
8) `dotnet add Program.Tests package NunitXml.TestLogger`

These commands will create a new solution file and add two new project files called `Program` and `Program.Tests`. `Program` is where the code we will be testing will live and `Program.Tests` is where we'll add our NUnit tests.

#### Adding Our .gitlab-ci.yml File

At the root directory of your repo create a new file called `.gitlab-ci.yml` with the following content:

    stages:
        - test
    
    variables:
        TEST_RESULTS_PATH: 'gitlab-nunit/Program.Tests/TestResults/test-run.xml'

    test:
    stage: test
    image: mcr.microsoft.com/dotnet/core/sdk:3.1
    script:
        - cd gitlab-nunit
        - dotnet restore && dotnet test --logger "nunit;LogFileName=test-run.xml"
    artifacts:
        when: always
        paths:
            - $TEST_RESULTS_PATH
        expire_in: 1 day

We will be adding more to this file later. I won't go into detail on what all the code here is doing but if you would like to learn more about Gitlab CI/CD check out their documentation here ([https://docs.gitlab.com/ee/ci/](https://docs.gitlab.com/ee/ci/))

#### Adding Our Testable Class

Inside `Program` a class should have been created called `Class1.cs`, rename this to `Calculator.cs` and paste over the existing code with the following code:

    namespace Program
    {
        public class Calculator
        {
            public int Add(int num1, int num2)
            {
                return num1 + num2;
            }
        }
    }

This will be the code we write our xUnit test for.

#### Adding Our xUnit Test

Inside of `Program.Tests` there will be a `UnitTest1.cs` file, rename this to `CalculatorTests.cs` and paste over the existing code with the following code:

    using NUnit.Framework;
    using Program;

    namespace program.tests
    {
        [TestFixture]
        public class CalculatorTests
        {
            private Calculator calculator;
    
            [SetUp]
            public void SetUp() {
                calculator = new Calculator();
            }
    
            [Test]
            public void Add_OnePlusOne_ReturnsTwo()
            {
                var expected = 2;
                var actual = calculator.Add(1, 1);
                    
                Assert.AreEqual(expected, actual);
            }
        }
    }

After following all the steps you should have a file structure that looks something like this:

    .gitlab-ci.yml
    /gitlab-xunit
        gitlab-xunit.sln
        /Program
            Program.csproj
            Calculator.cs
        /Program.Tests
            Program.Tests.csproj
            CalculatorTests.cs

Now to run our test we can run the `dotnet test` from the command line and we should see the following output:

    Starting test execution, please wait...
    
    A total of 1 test files matched the specified pattern.
    
    Test Run Successful.
    Total tests: 1
    Passed: 1
    Total time: 1.1043 Seconds

Great! Now that we've got our project set up with our first xUnit test running we can move onto modifying the `.gitlab-ci.yml` file to upload our test results to Obsidian!

#### Uploading to Obsidian

Before we modify our `.gitlab-ci.yml` make sure you have a few things setup that you will need for uploading your test results to Obsidian:
1) An Obsidian account (you can create one at [Obsidian QA](https://www.obsidianqa.com))
2) A project created in Obsidian (you will need to store the project ID in the YAML below)
3) An environment with which to associate this test run to (you will need to store the environment ID in the YAML below)
4) An API key (you will need the API key and secret) See [Gitlab docs](https://docs.gitlab.com/ee/security/cicd_environment_variables.html) for more on how to store this type of info securely in Gitlab

Once we have those things done we're ready to move on!

Let's start by adding the following to our existing `.gitlab-ci.yml` file we created earlier:

    # THESE CHANGES ARE ADDITIVE #
    # Make sure to ADD them to your existing stages and variables blocks in your YAML. 
    
    stages:
        - report

    variables:
        OBSIDIAN_PROJECT_ID: 'abc123'     # Swap this out for your real value
        OBSIDIAN_ENVIRONMENT_ID: 'abc123' # Swap this out for your real value
        OBSIDIAN_API_KEY: 'abc123'        # Swap this out for your real value
        OBSIDIAN_API_SECRET_KEY: 'abc123' # Swap this out for your real value

    upload-test-runs:
        stage: report
        image: node:14-alpine
        when: always
        variables:
            OBSIDIAN_MAPPER: 'obsidian-nunit-mapper'
        script:
            - yarn global add obsidian-cli $OBSIDIAN_MAPPER
            - |
            obsidian testruns import \
                --file $TEST_RESULTS_PATH \
                --mapper $OBSIDIAN_MAPPER \
                --environment $OBSIDIAN_ENVIRONMENT_ID \
                --project $OBSIDIAN_PROJECT_ID \
                --apikey $OBSIDIAN_API_KEY \
                --apisecret $OBSIDIAN_API_SECRET_KEY

You can view the full `.gitlab-ci.file` here: [https://gitlab.com/bitmischief/obsidian/tutorials/gitlab-nunit/-/blob/master/.gitlab-ci.yml](https://gitlab.com/bitmischief/obsidian/tutorials/gitlab-nunit/-/blob/master/.gitlab-ci.yml)

Commit your changes to your repository and your new pipeline should run. Now login to Obsidian QA and you should see your newly imported test run!
